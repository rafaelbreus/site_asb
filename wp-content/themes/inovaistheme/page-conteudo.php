<?php get_header(); ?>

<section id="content">

	<div class="container row">

		<div class="col l8 corpo">
			<div class="bc">
	          <ul>
		        <li><a href="">HOME</a></li>
		        	<span class="bc-arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/arrow.svg" width="5.3px"></span>
		        <li><a href="">CONTEÚDO</a></li>
		      </ul>
			</div>


			<h1 class="inovais-dotted-border">CONTEÚDO DO CURSO</h1>
			<div class="infos-curso valign-wrapper hide-on-small-only">
				<div class="box-img-infos valign">
					<div class="img-infos-curso valign" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico_small.svg');">
					</div>
				</div>
				<div class="box-infos-curso valign">
					<div class="horas-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_horas.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="horas-infos-text"><b>CARGA HORÁRIA</b>&nbsp;&nbsp;<div class="thin">300 HORAS</div></div></span>
					</div>
					<div class="duracao-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_duracao.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="duracao-infos-text"><b>DURAÇÃO TOTAL</b>&nbsp;&nbsp;<div class="thin">6 MESES</div></div></span>
					</div>
					<div class="modulos-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_modulos.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="modulos-infos-text"><b>TOTAL DE MÓDULOS</b>&nbsp;&nbsp;<div class="thin">6 MÓDULOS</div></div></span>
					</div>
				</div>
			</div>

			<div class="hide-on-med-and-up infos-curso-mobile">

				<div class="box-infos-curso-mobile">
					<div class="horas-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_horas.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="horas-infos-text"><b>CARGA HORÁRIA</b>&nbsp;&nbsp;<div class="thin">300 HORAS</div></div></span>
					</div>
					<div class="duracao-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_duracao.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="duracao-infos-text"><b>DURAÇÃO TOTAL</b>&nbsp;&nbsp;<div class="thin">6 MESES</div></div></span>
					</div>
					<div class="modulos-infos-curso inovais-dotted-border">
						<span class="valign-wrapper"><img class="valign" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icon_modulos.svg" width="24px">&nbsp;&nbsp;&nbsp;<div class="modulos-infos-text"><b>TOTAL DE MÓDULOS</b>&nbsp;&nbsp;<div class="thin">6 MÓDULOS</div></div></span>
					</div>
				</div>
			</div>

			<div class="modulos-curso">


				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						1
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>CONCEITOS FUNDAMENTAIS NA ATUAÇÃO AMBULATORIAL</h2>
						<p>Unidade I - Conceitos fundamentais em antissepsia, desinfecção e esterilização de instrumentais</p>
						<p>Unidade II - Preparo do uniforme de trabalho e lavagem pré-cirúrgica das mãos</p>
						<p>Unidade III - Paramentação para atuação</p>
						<p>Unidade IV - Conhecendo os procedimentos cirúrgicos especializados</p>
						<p>Unidade V - Instrumentais cirúrgicos, planejamento e preparo de mesa</p>
						<p>Unidade VI - Preparo pré-cirúrgico do paciente</p>
						<p>Unidade VII - Anestesia local, sedação enteral e parenteral</p>
						<p>Unidade VIII - Prontuário clínico</p>
					</div>
				</div>

				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						2
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>ATUANDO NA CIRURGIA ORAL MENOR</h2>
						<p>Unidade I - Procedimentos ambulatoriais possíveis</p>
						<p>Unidade II - Instrumental específico</p>
					</div>
				</div>

				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						3
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>ATUANDO NA CIRURGIA DE IMPLANTES E ENXERTOS</h2>
						<p>Unidade I - Procedimentos ambulatoriais possíveis</p>
						<p>Unidade II - Instrumental específico</p>
					</div>
				</div>

				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						4
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>ATUANDO NAS URGÊNCIAS E EMERGÊNCIAS</h2>
						<p>Unidade I - Conceitos fundamentais</p>
						<p>Unidade II - Tipos de urgências e emergências</p>
					</div>
				</div>

				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						5
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>NOVAS RESOLUÇÕES E PROTOCOLOS DE ESTERILIZAÇÃO, DESINFECÇÃO E ANTESSEPSIA</h2>
						<p>Unidade I - Rotinas e normativas em ambiente ambulatorial</p>
						<p>Unidade II - Rotinas hospitalares</p>
					</div>
				</div>

				<div class="modulo inovais-dotted-border row">
				<div class="numero-modulo col l1 s12">
					<div class="numero center-align">
						6
					</div>
				</div>
					<div class="conteudo-modulo col l11 s12">
						<h2>ATUANDO NAS CIRURGIAS HOSPITALARES</h2>
						<p>Unidade I - Cirurgias menores</p>
						<p>Unidade II - Procedimentos específicos com necessidade de atuação hospitalar</p>
					</div>
				</div>








			</div>
		</div>

		<div class="col l4 sidebar-container">
			<?php include('sidebar.php'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
