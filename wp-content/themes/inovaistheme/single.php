<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section id="content">

	<div class="container row">

		<div class="col l8 m12 s12 corpo">
			<div class="bc">
	          <ul>
		        <li><a href="">HOME</a></li>
		        	<span class="bc-arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/arrow.svg" width="5.3px"></span>
		        <li><a href="">O QUE É</a></li>
		      </ul>
			</div>


			<h1 class="inovais-dotted-border"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<div class="post-image center-align">
				<img src="<?php the_post_thumbnail_url(); ?>" alt="" class="post-image-img">
			</div>

			<div class="post-text">
				<p><?php the_content(); ?></p>
			</div>
		<?php endwhile; else: ?>
			<div class="artigo">
				<h2>Nada Encontrado</h2>
				<p>Erro 404</p>
				<p>Lamentamos mas não foram encontrados artigos.</p>
			</div>
		<?php endif; ?>
		</div>

		<div class="col l4 m12 s12 sidebar-container">
			<?php include('sidebar.php'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
