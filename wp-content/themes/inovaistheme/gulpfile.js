/**
 * npm install
 */
var gulp          = require('gulp');
var gutil         = require('gulp-util');
var uglify        = require('gulp-uglify');
var clean         = require('gulp-clean');
var concat        = require('gulp-concat');
var sass          = require('gulp-sass');
var minifycss     = require('gulp-minify-css');
var order         = require("gulp-order");
var connect       = require('gulp-connect-php');
var image         = require('gulp-image');
var browserSync   = require('browser-sync');
var changed       = require('gulp-changed');
var imagemin      = require('gulp-imagemin');



var browserReload = browserSync.reload;

//js
gulp.task('scripts', function() {
  var js_dest = 'assets/dist/js';
  return gulp.src('assets/src/js/*')
      .pipe(order([                             // js ordem
        'assets/src/js/materialize.min.js',
        'assets/src/js/**/!(app)*.js',          // tudo menos o app.js
        'assets/src/js/app.js'
      ], { base: './' }))
      .pipe(concat('app.min.js'))               // contatenar
      .pipe(uglify())                           // minificar
      .pipe(gulp.dest(js_dest))                 // salvar
      .pipe(browserReload({stream: true}))
      .on('error', gutil.log);
});

//sass
gulp.task('sass', function () {
  var sass_dest = 'assets/src/css';
  return gulp
    // Find all `.scss` files from the `css/` folder
    .src('assets/src/sass/*.scss')
    // Run Sass on those files
    .pipe(sass())
    // Write the resulting compiled CSS in the output folder
    .pipe(gulp.dest(sass_dest))
    .pipe(browserReload({stream: true}))
    .on('error', gutil.log);
});

//css
gulp.task('css', function() {
  var css_dest = 'assets/dist/css';
  return gulp.src('assets/src/css/*')
      .pipe(order([                             // css ordem
        'assets/src/css/materialize.css',
        'assets/src/css/**/!(app)*.css',        // tudo menos o app.css
        'assets/src/css/app.css'
      ], { base: './' }))
      .pipe(concat('app.min.css'))              // contatenar
      .pipe(minifycss())                        // minificar
      .pipe(gulp.dest(css_dest))                // salvar
      .pipe(browserReload({stream: true}))
      .on('error', gutil.log);
});

//fonts
gulp.task('fonts', function() {
  return gulp.src('assets/src/fonts/**/*')
      .pipe(gulp.dest('assets/dist/fonts/'))   // salvar
      .pipe(browserReload({stream: true}))
      .on('error', gutil.log);
});

//images
gulp.task('image', function () {
  gulp.src('assets/src/img/**/*')
    .pipe(changed('assets/dist/img/'))
    .pipe(image())
    .pipe(imagemin({
            progressive: true
     }))
    .pipe(gulp.dest('assets/dist/img/'))       // salvar
    .pipe(browserReload({stream: true}))
    .on('error', gutil.log);

});

// clean all builds
gulp.task('clean', function() {
  return gulp.src(['assets/dist/'], {read: false})
    .pipe(clean());
});

// web server
gulp.task('webserver', function() {
  connect.server({}, function (){
    browserSync({
      proxy: '127.0.0.1:8000'
    });
  });
});

// task padrao
gulp.task('watch', function(){
  gulp.watch('assets/src/sass/**/*.scss', ['sass']);                           // escuta alteracoes de scss
  gulp.watch('assets/src/css/**/*.css', ['css']);                           // escuta alteracoes de css
  gulp.watch('assets/src/js/**/*.js', ['scripts']);                         // escuta alteracoes de js
  gulp.watch('assets/src/fonts/**/*', ['fonts']);                           // escuta alteracoes de fontes
  gulp.watch('assets/src/img/**/*', ['image']);                             // escuta alteracoes de imagens
  gulp.watch("**/*.php").on("change", browserReload);
});

gulp.task('default', ['webserver', 'sass', 'css', 'scripts', 'fonts', 'image', 'watch']);
