<?php get_header(); ?>


<section id="video">
	<div class="container">
		<div class="video inovais-dotted-border" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/video.jpg');">

		</div>
	</div>
</section>


<section id="informacoes">





	<div class="container row center-align">




		<?php

		$args = array(
			'post_type' => 'informacao',
			'posts_per_page' => 4,
		);


		// The Query
		$the_query = new WP_Query( $args );

		// The Loop
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); ?>



				<div class="col l3 m6 s12 center-align info">
				<a href="">
					<div class="img-info center" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
					</div>
					<h6><?php the_title() ?></h6>
					<hr>
					<p><?php the_content(); ?></p>
					<i class="material-icons more-horiz">more_horiz</i>
					<div class="right-align">
						<p class="ler-mais"><i class="material-icons add">add</i><span class="inovais-dotted-border">LER MAIS</span></p>
					</div>
					</a>
				</div>



		<?php

			}
			/* Restore original Post Data */
			wp_reset_postdata();
		} else {
			// no posts found
		}
		?>




	</div>
</section>

<section id="noticias">
	<div class="container row">
		<div class="inovais-dotted-border titulo-noticias"><span class="valign-wrapper">
			<i class="material-icons valign">speaker_notes </i> &nbsp; NOTÍCIAS
		</span></div>



			<div class="col l3 m6 s12 noticia">
			<a href="">
				<div class="img-noticia center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/img-capa-ceaico.jpg');">
				</div>
				<div class="texto-noticia">
					<h6 class="inovais-dotted-border" >CURSO ASB</h6>
					<p>CONHEÇA TAMBÉM O CURSO DE AUXILIAR EM SAÚDE BUCAL</p>
				</div>
				</a>
			</div>

			<div class="col l3 m6 s12 noticia">
			<a href="">
				<div class="img-noticia center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/img_blog.jpg');">
				</div>
				<div class="texto-noticia">
					<h6 class="inovais-dotted-border" >BLOG CEAICO</h6>
					<p>ENCONTRE SUGESTÕES, DICAS E INFORMAÇÕES SOBRE ODONTOLOGIA</p>
				</div>
				</a>
			</div>

			<div class="col l3 m6 s12 noticia">
			<a href="">
				<div class="img-noticia center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/img_webinar.jpg');">
				</div>
				<div class="texto-noticia">
					<h6 class="inovais-dotted-border" >WEBINAR GRÁTIS</h6>
					<p>CONHEÇA TODAS AS NOVIDADES ATRAVÉS DOS WEBINARS</p>
				</div>
				</a>
			</div>

			<div class="col l3 m6 s12 noticia">
			<a href="">
				<div class="img-noticia center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico_small.svg');">
				</div>
				<div class="texto-noticia">
					<h6 class="inovais-dotted-border" >CEAICO DE CARA NOVA</h6>
					<p>ESSE ANO O CEAICO ESTÁ DE CARA NOVA CONFIRA AS MUDANÇAS</p>
				</div>
				</a>
			</div>


	</div>
</section>
<?php get_footer(); ?>
