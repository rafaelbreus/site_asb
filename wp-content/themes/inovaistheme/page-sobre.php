<?php get_header(); ?>

<section id="content">

	<div class="container row">

		<div class="col l8 m12 s12 corpo">
			<div class="bc">
	          <ul>
		        <li><a href="">HOME</a></li>
		        	<span class="bc-arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/arrow.svg" width="5.3px"></span>
		        <li><a href="">O QUE É</a></li>
		      </ul>
			</div>


			<h1 class="inovais-dotted-border">O QUE É O CURSO CEAICO</h1>
			<div class="post-image center-align">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico_small.svg" alt="" width="160px">
			</div>

			<div class="post-text">
				<p>O <b>CEAICO</b> é uma plataforma pioneira no Brasil de cursos de especialização voltada aos profissionais auxiliares e técnicos da odontologia, medicina e enfermagem que queiram atuar com cirurgias odontológicas.
				Todos os cursos do <b>CEAICO</b> são realizados através do ensino à distância, permitindo flexibilidade nos horários de estudo e eficiência nos resultados. Através de vídeo-aulas, palestras ao vivo, discussões em fórum, trabalhos em grupo, materiais de leitura e demais atividades coordenadas por professores tutores, os quais humanizam o ensino à distância proporcionando excelência no aprendizado.</p>

				<h2>ESPECIALIZE-SE AGORA MESMO!</h2>
				<p>Não perca esta oportunidade de especializar-se no Auxílio e Instrumentação Cirúrgica em Odontologia. Nosso curso é certificado pela <b>ABO-PG</b> em todo o Brasil. Estude de casa ou do trabalho, no horário que for mais conveniente para você.</p>


				<h2>OBJETIVO</h2>
				<p>Capacitar o aluno para a obtenção de conhecimentos teóricos e práticos necessários à atividade de um Auxiliar e Instrumentador Cirúrgico em Odontologia, dentro dos princípios éticos e legais da profissão.</p>


				<h2>PÚBLICO-ALVO</h2>
				<p>Pessoas que já trabalham como ASBs, TSBs, Auxiliares de Enfermagem.</p>


				<h2>METODOLOGIA</h2>
				<p>O curso é realizado pela internet através de um ambiente virtual de aprendizagem, que é utilizado por muitas faculdades do país em educação a distância. A aprendizagem ocorre por meio de videoaulas, conteúdos explicativos e exercícios para fixação, com o acompanhamento de um tutor. O curso estimula a autoaprendizagem e a aproximação com as situações que o aluno vivencia diariamente em seu trabalho.</p>
			</div>
			<div class="cta-sobre center-align">
				<button class="inovais-btn-red">COMPRE ONLINE</button>
				<div class="cta-icones-pagamento"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icones_pagamento.svg" alt="" height="48px"></div>
			</div>
		</div>

		<div class="col l4 m12 s12 sidebar-container">
			<?php include('sidebar.php'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
