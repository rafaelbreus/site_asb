	<?php wp_footer(); ?>
	<?php
		$facebook = get_field('facebook', 'option');
		$instagram = get_field('instagram', 'option');
		$youtube = get_field('youtube', 'option');
	 ?>
	<section id="parceiros">
	  <div class="container center-align">
	    <h6>PARCEIROS OFICIAIS <b>ABO</b></h6>
	    <div class="parceiros center-align">
	        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_abo.png" width="200px">
	    </div>
	  </div>
	</section>

	<section id="ligue">
	  <div class="container center-align hide-on-med-and-up">
	    <h2>CLIQUE E LIGUE<br><b><?php the_field('telefone', 'option'); ?></b><br><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/maozinha.svg" width="42px" alt="flecha apontando para o telefone" style="transform: rotate(-90deg);"></h2>
	  </div>

	  <div class="container center-align hide-on-small-only">
	    <h2>LIGUE <b><?php the_field('telefone', 'option'); ?></b><br><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/maozinha.svg" width="42px" alt="flecha apontando para o telefone" style="transform: rotate(-90deg);"></h2>
	  </div>
	</section>

	<section id="formas-pagamento">
	    <div class="container row">
	    <div class="formas-de-pagamento center-align">
	      <p>FORMAS DE <b>PAGAMENTO</b></p>
	      <div class="icones-pagamento center-align">
	        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icone_12x.svg" style="display: inline-block;" height="30px">
	        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/cartoes.png" style="display: inline-block;" height="30px">
	      </div>
	    </div>
	  </div>
	</section>

	<section id="footer">
	  <div class="container">
	    <div class="row">
	      <div class="col l6 m8 s12">
	        <div class="logo-academis-site-seguro left-align">
	          <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_academis.svg" height="16px" alt="Academis EAD">
	          <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_site_seguro.svg" height="18px" alt="Site Seguro">
	        </div>
	      </div>
	      <div class="col l6 m4 s12">
	        <div class="small-social right-align">
						<?php if ($facebook): ?>
							<?php $facebook = get_field('facebook', 'option'); ?>
							<a href="<?php echo $facebook ?>" target="_blank" class="facebook">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_facebook.svg" width="26px" alt="Facebook" title="Facebook do Curso Ceaico">
							</a>
						<?php endif; ?>

						<?php if ($instagram): ?>
							<?php $instagram = get_field('instagram', 'option'); ?>
							<a href="<?php echo $instagram ?>" target="_blank" class="instagram">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_instagram.svg" width="26px" alt="Instagram" title="Instagram do Curso Ceaico">
							</a>
						<?php endif; ?>

						<?php if ($youtube): ?>
							<?php $youtube = get_field('youtube', 'option'); ?>
							<a href="<?php echo $youtube ?>" target="_blank" class="youtube">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_youtube.svg" width="26px" alt="Youtube" title="YouTube do Curso Ceaico">
							</a>
						<?php endif; ?>

	        </div>
	      </div>
	    </div>
	  </div>
	</section>
  <!-- js -->
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/dist/js/app.min.js"></script>

</body>
</html>
