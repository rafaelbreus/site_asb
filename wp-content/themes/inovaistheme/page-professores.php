<?php get_header(); ?>

<section id="content">

	<div class="container row">

		<div class="col l8 corpo">
			<div class="bc">
	          <ul>
		        <li><a href="">HOME</a></li>
		        	<span class="bc-arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/arrow.svg" width="5.3px"></span>
		        <li><a href="">PROFESSORES</a></li>
		      </ul>
			</div>


			<h1 class="inovais-dotted-border">PROFESSORES</h1>

			<div class="professores">

				<!-- Prof 1 -->
				<div class="professor row inovais-dotted-border">
					<div class="col l3 s12 center-align">
						<div class="foto-professor" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/dist/img/professor.jpg);"></div>
					</div>
					<div class="col l9 s12 dados-professor">
						<h2>ANTÔNIO CARLOS DOMINGUES DE SÁ</h2>
						<h6>DENTISTA</h6>

						<ul>
							<li>ESPECIALISTA EM CIRURGIA BUCO MAXILO FACIAL - USP - SP</li>
							<li>MESTRE EM CIRURGIA BUCO MAXILO FACIAL - USP-SP</li>
						</ul>

					</div>
				</div>

				<!-- Prof 2 -->

				<div class="professor row inovais-dotted-border">
					<div class="col l3 s12 center-align">
						<div class="foto-professor" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/dist/img/professor.jpg);"></div>
					</div>
					<div class="col l9 s12 dados-professor">
						<h2>FRAUZEMIR LOPES</h2>
						<h6>DENTISTA</h6>

						<ul>
							<li>PROFESSOR DA DISCIPLINA DE CIR. E TRAUM. BMF DA PUC-PR - (1984 / 1995)</li>
							<li>PROFESSOR DA DISCIPLINA DE CIR. E TRAUM. BMF DAS FACULDADES INGÁ (UNINGÁ- MARINGA-PR) – 2001 / 2003</li>
							<li>COORDENADOR DO CURSO DE ESPECIALIZAÇÃO EM CIR. E TRAUM. BMF DAS FACULDADES INGÁ (UNINGÁ – MARINGA–PR)- 2002 / 2004</li>
							<li>COORDENADOR DO CURSO DE ATUALIZAÇÃO EM EMERGÊNCIAS MÉDICAS EM ODONTOLOGIA E FORMALÃO DE SOCORRISTA INTERNACIONAL (UTP - ILAPEO)</li>
							<li>PROFESSOR DA DISCIPLINA DE EMERGÊNCIAS MÉDICAS EM ODONTOLOGIA DOS CURSOS DA ABO (PR E PG), SINDICATO DE ODONTOLOGIA, CINDACTA E UNINGA</li>
							<li>INSTRUTOR CREDENCIADO E MEMBRO DO NATIONAL SAFETY COUNCIL (NSC) PARA SUPORTE BÁSICO DE VIDA, PRIMEIROS SOCORROS E DESFIBRILADOR AUTOMATICO EXTERNO (DEA)</li>
							<li>INSTRUTOR CREDENCIADO DO EMERGENCY CARE AND SAFETY INSTITUTE (ECSI) PARA BLS, PRIMEIROS SOCORROS E (DEA)</li>
						</ul>

					</div>
				</div>

				<!-- Prof 3 -->

				<div class="professor row inovais-dotted-border">
					<div class="col l3 s12 center-align">
						<div class="foto-professor" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/dist/img/professor.jpg);"></div>
					</div>
					<div class="col l9 s12 dados-professor">
						<h2>JEFERSON DROPA</h2>
						<h6>DENTISTA</h6>

						<ul>
							<li>ESPECIALISTA EM CIRURGIA BUCO MAXILO FACIAL - USP - SP</li>
							<li>MESTRE EM CIRURGIA BUCO MAXILO FACIAL - USP-SP</li>
						</ul>

					</div>
				</div>



			</div>

</div>

		<div class="col l4 sidebar-container">
			<?php include('sidebar.php'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
