<?php

/**
 * Adiciona o suporte ao title-tag
 * @author Armando Tomazzoni Junior
 * @date 03/06/2016
 */
function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );

/**
 * Remove coisas inúteis do wp_head
 * @author Armando Tomazzoni Junior
 * @date 17/07/2014
 */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action('rest_api_init', 'wp_oembed_register_route');
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');

/**
 * Modificar a logomarca de login e do admin
 * @author Armando Tomazzoni Junior
 * @date 12/05/2014
 */
add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
  echo '<style type="text/css">
          #login h1 a {
            background: url("' . get_template_directory_uri() . '/assets/dist/img/logo_academis_verde.svg") no-repeat scroll center 0 transparent;
            width: 100%;
            height: 154px;
          }
          #nav, #backtoblog {
            display: none;
          }
       </style>';
}
add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
  echo '<style type="text/css">
  				#wp-admin-bar-wp-logo {display:none}
  			</style>';
}

/**
 * Remove permissão de criar novas páginas
 * @author Armando Tomazzoni Junior
 * @date 17/07/2014
 */
function permissions_admin_redirect() {
  $result = stripos($_SERVER['REQUEST_URI'], 'post-new.php?post_type=page');
  if ($result!==false && !current_user_can('publish_pages')) {
    wp_redirect(get_option('siteurl') . '/wp-admin/edit.php?post_type=page&permissions_error=true');
  }
}
add_action('admin_menu','permissions_admin_redirect');

function permissions_admin_notice() {
  echo "<div id='permissions-warning' class='error fade'><p><strong>".__('Você não tem permissão para criar novas páginas.')."</strong></p></div>";
}

function permissions_show_notice() {
  if($_GET['permissions_error']) {
    add_action('admin_notices', 'permissions_admin_notice');
  }
}
add_action('admin_init','permissions_show_notice');




/**
 * Remove a toolbar chata que aparece no site quando está logado no admin
 * @author Armando Tomazzoni Junior
 * @date 12/05/2014
 */
add_filter('show_admin_bar', '__return_false');

/**
 * Cria novos tipos de usuários (roles) e remove os inúteis
 * @author Armando Tomazzoni Junior
 * @date 17/07/2014
*/
add_role( 'cliente', 'Cliente');
remove_role('editor');
remove_role('file_uploader');
remove_role('author');
remove_role('contributor');
remove_role('subscriber');

global $wp_roles;
$role = get_role('cliente');
$role->add_cap( 'read' );
$role->add_cap( 'edit_files' );                     //upload de arquivos
$role->add_cap( 'upload_files' );                   //upload de arquivos
$role->add_cap( 'remove_upload_files' );            //upload de arquivos
$role->add_cap( 'delete_posts' );                   //upload de arquivos
$role->add_cap( 'edit_pages' );                     //edicao de paginas
$role->add_cap( 'edit_others_pages' );              //edicao de paginas
$role->add_cap( 'edit_published_pages' );           //edicao de paginas
$role->add_cap( 'edit_others_posts' );              //edicao de paginas
$role->add_cap( 'edit_posts' );                     //edicao de posts
$role->add_cap( 'edit_published_posts' );           //edicao de posts
$role->add_cap( 'publish_posts' );                  //edicao de posts
$role->add_cap( 'delete_others_posts' );            //upload de posts
$role->add_cap( 'delete_published_posts' );         //upload de posts



/*
 * Remove tamanhos de imagens desnecessárias
 * @author Armando Tomazzoni Junior
 * @date 12/05/2014
 */
add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
function remove_default_image_sizes( $sizes) {
  //unset( $sizes['thumbnail']); //300x300
  unset( $sizes['medium']); // 600x600
  //unset( $sizes['large']); // 1400x1400
  return $sizes;
}

/**
 * Remove links da adminbar
 * @author Armando Tomazzoni Junior
 * @date 19/03/2015
*/
function remove_admin_bar_links() {
  global $wp_admin_bar, $current_user;
  $wp_admin_bar->remove_menu('updates');
  $wp_admin_bar->remove_menu('comments');
  $wp_admin_bar->remove_menu('new-content');
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

function remove_links_menu() {
  global $menu;
  remove_menu_page('edit.php'); // CASO O SITE POSSUA BLOG, REMOVA ESSA LINHA
  remove_menu_page('upload.php');
  remove_menu_page('link-manager.php');
  remove_menu_page('tools.php');
  remove_menu_page('edit-comments.php');

  if(!current_user_can('publish_pages')) {
    remove_menu_page('profile.php');
  }
}
add_action( 'admin_menu', 'remove_links_menu' );

/**
 * Remove a metabox de atributos da página
 * @author Armando Tomazzoni Junior
 * @date 19/03/2015
 */
function remove_page_attribute_support() {
  remove_post_type_support('page','page-attributes');
}
add_action( 'init', 'remove_page_attribute_support' );

/**
 * Remove widgets da dashboard
 * @author Armando Tomazzoni Junior
 * @date 19/03/2015
*/
function disable_default_dashboard_widgets() {
  remove_action('welcome_panel', 'wp_welcome_panel');
  remove_meta_box('dashboard_right_now', 'dashboard', 'core');
  remove_meta_box('dashboard_activity', 'dashboard', 'core');
  remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');
  remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
  remove_meta_box('dashboard_plugins', 'dashboard', 'core');
  remove_meta_box('dashboard_quick_press', 'dashboard', 'core');
  remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');
  remove_meta_box('dashboard_primary', 'dashboard', 'core');
  remove_meta_box('dashboard_secondary', 'dashboard', 'core');
}
add_action('admin_menu', 'disable_default_dashboard_widgets');





function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
     )
   );
 }
 add_action( 'init', 'register_my_menus' );






/**
 * Corrige problema na acentuação das imagens
 * @author Armando Tomazzoni Junior
 * @date 13/07/2016
*/
add_filter('wp_get_attachment_image_attributes', 'ck_image_attrs');
function ck_image_attrs($attrs) {
  foreach ($attrs as $name => $value) {
    if ('src' != $name) {
      break;
    }
    $attrs[$name] = ck_fix_image_url($value);
  }
  return $attrs;
}

function ck_fix_image_url($url) {
  $parts = parse_url($url);
  $path = explode('/', $parts['path']);
  $path = array_map('rawurlencode', $path);
  $path = implode('/', $path);
  return str_replace($parts['path'], $path, $url);
}
