<?php
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Opções do tema',
		'menu_title'	=> 'Opções do tema',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Opções Header',
		'menu_title'	=> 'Opções Header',
		'parent_slug'	=> 'theme-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Opções Footer',
		'menu_title'	=> 'Opções Footer',
		'parent_slug'	=> 'theme-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Dados do site',
		'menu_title'	=> 'Dados do site',
		'parent_slug'	=> 'theme-options',
	));

}

/**
 * Registro do custom post type proxturma
 */
add_action( 'init', 'register_cpt_informacao' );

function register_cpt_informacao() {

	$labels = array(
		'name' => __( 'Informações', 'informacao' ),
		'singular_name' => __( 'Informação', 'informacao' ),
		'add_new' => __( 'Add nova informação', 'informacao' ),
		'add_new_item' => __( 'Add nova informação', 'informacao' ),
		'edit_item' => __( 'Editar informacao', 'informacao' ),
		'new_item' => __( 'Nova informacao', 'informacao' ),
		'view_item' => __( 'Ver informacao', 'informacao' ),
		'search_items' => __( 'Buscar informações', 'informacao' ),
		'not_found' => __( 'Nenhuma informacao encontrada', 'informacao' ),
		'not_found_in_trash' => __( 'Nenhuma informacao encontrada', 'informacao' ),
		'menu_name' => __( 'Informações', 'informacao' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'description' => 'Cadastro de informações',
		'supports' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => 'informacao',
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'rewrite' => array( 'slug' => 'informacao', 'with_front' => false ),
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'thumbnail')
	);

	register_post_type( 'informacao', $args );
}


add_theme_support( 'post-thumbnails' );
