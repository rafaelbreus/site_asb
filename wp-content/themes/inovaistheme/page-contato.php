<?php get_header(); ?>
<?php
		$data = get_field('data', 'option');
		$dia = substr($data, 0, 2);
		$mes = substr($data, 2, 3);
		wp_reset_postdata();
?>
<section id="contato">

	<div class="container row">
			<div class="bc">
	          <ul>
		        <li><a href="">HOME</a></li>
		        	<span class="bc-arrow"><img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/arrow.svg" width="5.3px"></span>
		        <li><a href="">CONTATO</a></li>
		      </ul>
			</div>
			<br>
			<br>
		<div class="col l6 s12">

			<div class="form-contato">

				<?php echo do_shortcode( '  [contact-form-7 id="58" title="Formulário de contato 1"]  ' ); ?>

		        <!-- <div class="input-field">
		          <i class="material-icons prefix">person</i>
		          <input id="icon_prefix" placeholder="NOME COMPLETO" type="text" class="validate">
		        </div>

		        <div class="input-field">
		          <i class="material-icons prefix">mail</i>
		          <input id="icon_prefix" placeholder="EMAIL" type="email" class="validate">
		        </div>

		        <div class="input-field">
		          <i class="material-icons prefix">phone</i>
		          <input id="icon_prefix" placeholder="TELEFONE FIXO" type="tel" class="validate input-fone">
		        </div>

		        <div class="input-field">
		          <i class="material-icons prefix">subject</i>
		          <textarea id="icon_prefix" placeholder="MENSAGEM" type="tel" class="validate"></textarea>
		        </div>

		       	<div class="right-align">
		       		<button class="btn-enviar" type="submit" name="action"> ENVIAR MENSAGEM</button>
		       	</div> -->
			</div>

		</div>

		<div class="col l6 s12">
			<div class="row precos">
				<div class="col l2 s3">
		          <div class="calendario calendario-precos center-align">
		            <div class="data">
									<h5 class="dia"><?php echo $dia ?></h5>
	                <h6 class="mes"><?php echo $mes ?></h6>
		            </div>
		          </div>
				</div>
				<div class="col l10 s9">
					<img class="logo-ceaico-preto" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico_completo.svg" width="215px" alt="Curso Ceaico"><br>
					<img class="logo-academis-preto" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_academis_verde.svg" width="120px" alt="Academis EAD">
					<h6>RECONHECIDO <b>ABO</b> E <b>CFO</b></h6>
					<p class="inovais-dotted-border valor" >VALOR DO CURSO <b>R$ 990,00</b></p>
					<h4>OPÇÃO 1</h4>
					<h5>NO <b>0800-052-2222</b> OU ATRAVÉS DA <b>COMPRA ONLINE</b> PARCELADO EM ATÉ <b>12X</b> DE <b>R$ 98,50</b> NO CARTÃO DE CRÉDITO <b>(SEM JUROS)</b></h5>

					<h4>OPÇÃO 2</h4>
					<h5>ATRAVÉS DO <b>0800-052-2222</b> PARCELADO EM ATÉ <b>7X</b> DE <b>R$ 169,00</b> <b>(SEM JUROS)</b></h5>




				</div>

			</div>

		</div>
	</div>
</section>

<?php get_footer(); ?>
