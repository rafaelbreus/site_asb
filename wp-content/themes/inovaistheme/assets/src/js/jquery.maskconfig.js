$(document).ready(function() {

  /**
   * Configuração de máscaras nos inputs
   * @author Armando Tomazzoni Jr.
   */
  var SPMaskBehavior = function (val) {
  	// padrão telefone com 9 digitos
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },
  spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
  };

  var NineDigitMaskBehavior = function (val) {
    // padrão RG com 9 digitos
    return val.replace(/\D/g, '').length === 9 ? '00.000.000-0' : '0.000.000-09';
    
  },
  NDOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(NineDigitMaskBehavior.apply({}, arguments), options);
      }
  };

  /**
   * Configuração de máscara nos inputs
   * @author Armando Tomazzoni Junior
   * @date 03/05/2016
   */
  $('.input-fone').mask(SPMaskBehavior, spOptions);
  $('.input-cpf').mask('000.000.000-00');
  $('.input-cnpj').mask('00.000.000/0000-00');
  $('.input-cep').mask('00000-000');
  $(".input-rg").mask(NineDigitMaskBehavior, NDOptions);
  $('.input-data').mask('00/00/0000');


  
});