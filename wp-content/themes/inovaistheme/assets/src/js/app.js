$(document).ready(function() {


  var url = window.location.href;
  var activePage = url;
  $('.full-menu a').each(function () {
      var linkPage = this.href;

      if (activePage == linkPage) {
          $(this).closest("li").addClass("current");
      }
  });



	/**
	 * MaterializeCSS
	 */
  $('.slider').slider();
  $('.scrollspy').scrollSpy();
  $('.button-collapse').sideNav({
    closeOnClick: true,
    edge: 'left'
  });




});
