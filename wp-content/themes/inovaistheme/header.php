<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <!-- metas -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/dist/img/favicon.png" />
    <meta name="google-site-verification" content="" />

    <!-- seo -->
    <title>Curso Ceaico</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="DF Systems" />
    <meta name="copyright" content="DF Systems" />

    <!-- css -->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dist/css/app.min.css" />

    <!-- wordpress -->
    <?php wp_head(); ?>


  </head>



<body <?php body_class(); ?>>
  <?php
      $facebook = get_field('facebook', 'option');
      $instagram = get_field('instagram', 'option');
      $youtube = get_field('youtube', 'option');
      $data = get_field('data', 'option');
      $dia = substr($data, 0, 2);
      $mes = substr($data, 2, 3);
      wp_reset_postdata();
  ?>
  <header>
    <!-- Barra cinza do topo com os icones sociais -->
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col l6 m6 s6">
            <div class="small-logo left-align">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico.svg" width="40px" alt="Curso Ceaico">
              </a>

            </div>
          </div>
          <div class="col l6 m6 s6">
            <div class="small-social right-align">
              <?php if ($facebook): ?>
                <?php $facebook = get_field('facebook', 'option'); ?>
                <a href="<?php echo $facebook ?>" target="_blank" class="facebook">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_facebook.svg" width="26px" alt="Facebook" title="Facebook do Curso Ceaico">
                </a>
              <?php endif; ?>

              <?php if ($instagram): ?>
                <?php $instagram = get_field('instagram', 'option'); ?>
                <a href="<?php echo $instagram ?>" target="_blank" class="instagram">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_instagram.svg" width="26px" alt="Instagram" title="Instagram do Curso Ceaico">
                </a>
              <?php endif; ?>

              <?php if ($youtube): ?>
                <?php $youtube = get_field('youtube', 'option'); ?>
                <a href="<?php echo $youtube ?>" target="_blank" class="youtube">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_youtube.svg" width="26px" alt="Youtube" title="YouTube do Curso Ceaico">
                </a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fim da barra cinza do topo com os icones sociais -->

    <!-- Conteúdo do header -->
    <div class="header-content">
      <div class="container">
        <div class="row">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <div class="logo-completo col l4 m6 s12">

              <div class="calendario calendario-logo center-align">
                <div class="data">
                  <h5 class="dia"><?php echo $dia ?></h5>
                  <h6 class="mes"><?php echo $mes ?></h6>
                </div>
              </div>


            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_ceaico_completo.svg" width="100%" alt="Curso Ceaico">
          </div>
          </a>
          <div class="logo-academis-ead-verde col l5 m6 s12">
            <a href="http://www.inovais.com" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo_academis_verde.svg" width="120px" alt="Academis EAD">
            </a>
            <h5>CURSO DE FORMAÇÃO NO AUXÍLIO EM INSTRUMENTAÇÃO<br>CIRÚRGICA EM ODONTOLOGIA</h5>
            <h6>RECONHECIDO <b>ABO</b></h6>
          </div>
          <div class="col l3 m12 s12">
            <div class="telefone center-align">
              <div class="maozinha">
                <div class='angrytext' >
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/maozinha.svg" width="36px" alt="flecha apontando para o telefone">
                </div>
              </div>
              <div class="texto-telefone">
                <h5 class="compre-telefone">COMPRE PELO TELEFONE</h5>
                <h3 class="numero-telefone inovais-dotted-border"><?php the_field('telefone', 'option'); ?></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fim do conteúdo do header -->

    <!-- Menu principal -->

    <div class="container">
      <div class="menu valign-wrapper inovais-dotted-border">
        <!-- Dropdown Trigger -->
        <a class='dropdown-button' href='#' data-activates='dropdown1'> MENU</a>

        <!-- Dropdown Structure -->
        <ul id='dropdown1' class='dropdown-content'>
          <li><a href="<?php echo get_site_url(); ?>/">HOME</a></li>
          <li><a href="<?php echo get_site_url(); ?>/sobre">O QUE É</a></li>
          <li><a href="<?php echo get_site_url(); ?>/conteudo">CONTEÚDO</a></li>
          <li><a href="<?php echo get_site_url(); ?>/blog">BLOG</a></li>
          <li><a href="<?php echo get_site_url(); ?>/professores">PROFESSORES</a></li>
          <li><a href="<?php echo get_site_url(); ?>/contato">CONTATO</a></li>
        </ul>

        <ul class="valign full-menu">
          <li><a href="<?php echo get_site_url(); ?>//">HOME</a></li>
          <li><a href="<?php echo get_site_url(); ?>/sobre">O QUE É</a></li>
          <li><a href="<?php echo get_site_url(); ?>/conteudo">CONTEÚDO</a></li>
          <li><a href="<?php echo get_site_url(); ?>/blog">BLOG</a></li>
          <li><a href="<?php echo get_site_url(); ?>/professores">PROFESSORES</a></li>
          <li><a href="<?php echo get_site_url(); ?>/contato">CONTATO</a></li>
        </ul>
        <div class="header-button-wrapper"><button class="inovais-btn-red" >COMPRE ONLINE</button></div>
      </div>
    </div>
    <!-- Fim do menu principal -->

  </header>

  <section id="proxima-turma">
<div class="container">
  <div class="box-proxima-turma inovais-dotted-border" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/dist/img/bg_proxima_turma.png');">
    <div class="row">
      <div class="col l5">
      <div class="calendar-pos">
        <div class="calendario calendario-turma center-align">
          <div class="data">
            <h5 class="dia"><?php echo $dia ?></h5>
            <h6 class="mes"><?php echo $mes ?></h6>
          </div>
        </div>
      </div>

      </div>
      <div class="col l3 valign-wrapper">
        <div class="valign cta-proxima-turma">
          <h5><b>PRÓXIMA TURMA</b> CEAICO ONLINE</h5>
          <button class="inovais-btn-red">COMPRE ONLINE</button>
        </div>
      </div>
      <div class="col l4 valign-wrapper">
        <div class="valign">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/icones_pagamento.svg" width="180px">
        </div>

      </div>
    </div>
  </div>
</div>
</section>
