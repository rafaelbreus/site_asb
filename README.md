# README #

Passos para dev do site Curso ASB

### Requisitos ###

* Ter o Node.JS instalado na máquina ([https://nodejs.org/en/](Link URL))
* Ter o Gulp instalado. (Executar no terminal o comando: ```npm install -g gulp```)
* Ter o banco de dados MySQL do Wordpress configurado corretamente na máquina


### Rodando o serviço do Gulp (minificar e buildar site) ###

* Pelo terminal, navegar até a pasta do tema: ```cd diretorio_do_site/wp-content/themes/nome_do_tema```
* Dentro da pasta, executar o comando para buildar: ```gulp```

**Não subir para o servidor a pasta ```node_modules``` e nem a pasta ```dist/src```**